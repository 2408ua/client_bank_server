package com.example.client_bank_server.dao;

import com.example.client_bank_server.entity.Account;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AccountDao implements Dao<Account>{
  private final List<Account> accounts;

  public AccountDao(List<Account> accounts) {
    this.accounts = accounts;
  }

  private int getIndex(int id) {
    return id - 1;
  }

  private int getNextId() {
    if (accounts.size() == 0) return 1;
    return accounts.get(accounts.size() - 1).getId() + 1;
  }

  @Override
  public Account save(Account account) {
    account.setId(getNextId());
    accounts.add(account);
    return account;
  }

  @Override
  public boolean delete(Account obj) {
    return false;
  }

  @Override
  public void deleteAll(List<Account> entities) {

  }

  @Override
  public void saveAll(List<Account> entities) {

  }

  @Override
  public List<Account> findAll() {
    return null;
  }

  public List<Account> findAllByCustomerId(int id) {
    return accounts.stream()
        .filter(a -> a.getCustomerId() == id)
        .collect(Collectors.toList());
  }

  @Override
  public boolean deleteById(int id) {
    return false;
  }

  @Override
  public Account getOne(int id) {
    return null;
  }
}
