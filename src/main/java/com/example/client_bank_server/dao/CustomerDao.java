package com.example.client_bank_server.dao;

import com.example.client_bank_server.entity.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDao implements Dao<Customer> {
  private final List<Customer> customers;

  public CustomerDao(List<Customer> customers) {
    this.customers = customers;
  }

  private int getIndex(int id) {
    return id - 1;
  }

  private int getNextId() {
    if (customers.size() == 0) return 1;
    return customers.get(customers.size() - 1).getId() + 1;
  }

  @Override
  public Customer save(Customer customer) {
    customer.setId(getNextId());
    customers.add(customer);
    return customer;
  }

  @Override
  public boolean delete(Customer obj) {
    return false;
  }

  @Override
  public void deleteAll(List<Customer> entities) {

  }

  @Override
  public void saveAll(List<Customer> entities) {

  }

  public Customer updateById(int id, Customer updatedCustomer) {
    customers.set(getIndex(id), updatedCustomer);
    return customers.get(getIndex(id));
  }

  @Override
  public List<Customer> findAll() {
    return customers;
  }

  @Override
  public boolean deleteById(int id) {
    Customer remove = customers.remove(getIndex(id));
    return remove != null;
  }

  @Override
  public Customer getOne(int id) {
    return customers.get(getIndex(id));
  }
}
