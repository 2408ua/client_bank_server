package com.example.client_bank_server.controller;

import com.example.client_bank_server.entity.Account;
import com.example.client_bank_server.entity.Customer;
import com.example.client_bank_server.service.AccountService;
import com.example.client_bank_server.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {
  private final CustomerService customerService;
  private final AccountService accountService;

  public CustomerController(CustomerService customerService, AccountService accountService) {
    this.customerService = customerService;
    this.accountService = accountService;
  }

  @GetMapping
  public List<Customer> getCustomers() {
    return customerService.getCustomers();
  }

  @GetMapping("/{id}")
  public Customer getCustomerById(@PathVariable("id") int id) {
    return customerService.getCustomerById(id);
  }

  @PostMapping
  public Customer saveCustomer(@RequestBody Customer customer) {
    return customerService.saveCustomer(customer);
  }

  @PutMapping("/{id}")
  public Customer updateCustomerById(@PathVariable("id") int id, @RequestBody Customer customer) {
    return customerService.updateCustomer(id, customer);
  }

  @DeleteMapping("/{id}")
  public boolean deleteCustomerById(@PathVariable("id") int id) {
    return customerService.deleteCustomerById(id);
  }

  @PostMapping("/{id}/account")
  public Account addCustomerAccount(@PathVariable("id") int id, @RequestBody Account account) {
    return accountService.addAccount(account, id);
  }

  @DeleteMapping("/{id}/account/{accountId}")
  public void deleteCustomerAccount(@PathVariable("id") int id, @PathVariable("accountId") int accountId) {

  }
}
