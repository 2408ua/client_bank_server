package com.example.client_bank_server.controller;

import com.example.client_bank_server.service.AccountService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
public class AccountController {
  private final AccountService accountService;

  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @PostMapping("/{id}/sum/{sum}")
  public void topUpAccount(@PathVariable("id") int id, @PathVariable("sum") int sum) {

  }

  @GetMapping("/{id}")
  public void withdrawMoney(@PathVariable("id") int id) {

  }

  @PostMapping("/{id}/transferAccount/{accountId}")
  public void transferMoney(@PathVariable("id") int id, @PathVariable("accountId") int accountId) {

  }
}
