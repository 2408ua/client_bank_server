package com.example.client_bank_server.service;

import com.example.client_bank_server.dao.AccountDao;
import com.example.client_bank_server.dao.CustomerDao;
import com.example.client_bank_server.entity.Account;
import com.example.client_bank_server.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {
  private final CustomerDao customerDao;
  private final AccountDao accountDao;

  public CustomerService(CustomerDao customerDao, AccountDao accountDao) {
    this.customerDao = customerDao;
    this.accountDao = accountDao;
  }

  public List<Customer> getCustomers() {
    List<Customer> customers = customerDao.findAll();
    customers.forEach(customer -> {
      List<Account> allByCustomerId = accountDao.findAllByCustomerId(customer.getId());
      customer.setAccounts(allByCustomerId);
    });
    return customers;
  }

  public Customer getCustomerById(int id) {
    return customerDao.getOne(id);
  }

  public Customer saveCustomer(Customer customer) {
    return customerDao.save(customer);
  }

  public Customer updateCustomer(int id, Customer customer) {
    Customer updatedCustomer = customerDao.getOne(id);
    String newName = customer.getName();
    String newEmail = customer.getEmail();
    Integer newAge = customer.getAge();
    List<Account> newAccount = customer.getAccounts();

    if (newName != null) updatedCustomer.setName(newName);
    if (newEmail != null) updatedCustomer.setEmail(newEmail);
    if (newAge != null) updatedCustomer.setAge(newAge);
    if (newAccount != null) updatedCustomer.setAccounts(newAccount);

    return customerDao.updateById(id, updatedCustomer);
  }

  public boolean deleteCustomerById(int id) {
    return customerDao.deleteById(id);
  }
}
