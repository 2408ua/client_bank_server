package com.example.client_bank_server.service;

import com.example.client_bank_server.dao.AccountDao;
import com.example.client_bank_server.entity.Account;
import com.example.client_bank_server.entity.Customer;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
  private final AccountDao accountDao;

  public AccountService(AccountDao accountDao) {
    this.accountDao = accountDao;
  }

  public void topUpAccount(String number, int sum) {

  }

  public void withdrawAccount(String number, int sum) {

  }

  public void transferMoney(String number1, String number2, int sum) {
  }

  public Account addAccount(Account account, int id) {
    account.setCustomerId(id);
    return accountDao.save(account);
  }
}
