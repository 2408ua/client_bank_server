package com.example.client_bank_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientBankServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ClientBankServerApplication.class, args);
  }

}
