package com.example.client_bank_server.entity;

public enum Currency {
  USD,
  EUR,
  UAH,
  CHF,
  GBP
}
