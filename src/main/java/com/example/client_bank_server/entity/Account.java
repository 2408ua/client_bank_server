package com.example.client_bank_server.entity;

import java.util.Objects;
import java.util.UUID;

public class Account {
  private Integer id;
  private Double balance = 0.0;
  private final String number;
  private final Currency currency;
  private Integer customerId;

  public Account(Currency currency, Customer customer) {
    this.number = UUID.randomUUID().toString();
    this.currency = currency;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setBalance(Double balance) {
    this.balance = balance;
  }

  public void setCustomerId(int id) {
    this.customerId = id;
  }

  public int getId() {
    return id;
  }

  public Double getBalance() {
    return balance;
  }

  public String getNumber() {
    return number;
  }

  public Currency getCurrency() {
    return currency;
  }

  public int getCustomerId() {
    return customerId;
  }

  @Override
  public String toString() {
    return "Account{" +
        "id=" + id +
        ", balance=" + balance +
        ", number='" + number + '\'' +
        ", currency=" + currency +
        ", customerId=" + customerId +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Account account = (Account) o;
    return Objects.equals(id, account.id) && Objects.equals(balance, account.balance) && Objects.equals(number, account.number) && currency == account.currency && Objects.equals(customerId, account.customerId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, balance, number, currency, customerId);
  }
}
