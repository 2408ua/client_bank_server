package com.example.client_bank_server.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Customer {
  private int id;
  private String name;
  private String email;
  private Integer age;
  private List<Account> accounts;

  public Customer(String name, String email, Integer age) {
    this.name = name;
    this.email = email;
    this.age = age;
    this.accounts = new ArrayList<>();
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public void setAccounts(List<Account> accounts) {
    this.accounts = accounts;
  }

  public void saveAccount(Account account) {
    this.accounts.add(account);
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public Integer getAge() {
    return age;
  }

  public List<Account> getAccounts() {
    return accounts;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        ", age=" + age +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Customer customer = (Customer) o;
    return Objects.equals(id, customer.id) && Objects.equals(name, customer.name) && Objects.equals(email, customer.email) && Objects.equals(age, customer.age) && Objects.equals(accounts, customer.accounts);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, email, age, accounts);
  }
}
